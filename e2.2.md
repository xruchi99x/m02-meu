# E2. Exercici 2. Processat de fitxers

## Introducció

Una de les grans ventatges de l'entorn UNIX és la facilitat per a processar fitxers, especialment fitxers amb dades, i transformar-les.

## Continguts

Feu servir el help i el manual per a cada comanda

### cut

La comanda cut ens permet extreure camps, per defecte separats per tabuladors, d'un llistat. Per exemple si tenim en un fitxer:

```
one	two	three	four	five
alpha	beta	gamma	delta	epsilon
```

Amb la comanda ```cut -f 3 data.txt``` obtindrem:

```
three
gamma
```

que són els camps en tercera posició

### sort

Ens permetrà ordenar-los. Per exemple, si tenim:

```
apples
oranges
pears
kiwis
bananas
```

I fem:

```
sort data.txt
```

Obtindrem:

```
apples
bananas
kiwis
oranges
pears
```

### uniq

Ens permet filtrar línies repetides. Per exemple si tenim:

```
This is a line.
This is a line.
This is a line.
 
This is also a line.
This is also a line.
 
This is also also a line.
```

I executem **uniq data.txt** obtindrem:

```
This is a line.
 
This is also a line.
 
This is also also a line.
```

## Entrega

1. Llisteu els noms dels usuaris connectats al sistema.    
who  
2. Extraieu els camps 1 i 3 del resultat de l'ordre who.  
who | tr -s ' ', ' ' | cut -s -d ' ' -f1,3    
3. Llisteu els permisos i el nom de tots els vostres fitxers situats en el directori HOME.  
ls -lisa  
4. Llisteu el propietari i grandària de tots els fitxers del directori HOME.  
ls -hlsa /home | tr -s ' ', ' ' | cut -s -d ' ' -f1,5  
5. Comproveu si existeix o no una entrada del vostre usuari en el fitxer /etc/passwd 
cat /etc/passwd    
si hi es. 
6. Llisteu tots els usuaris del mateix grup que vosaltres que existeixin en aquest fitxer.  
cat /etc/passwd | tr -s ':', ' ' | cut -s -d' ' -f3 | sort  
7. Llisteu els identificadors d'usuari  
cat /etc/passwd | tr -s ':', ' ' | cut -s -d' ' -f1 | sort  
8. LListeu els shell usats pels usuaris.  
cat /etc/passwd | tr -s ':', ' ' | cut -s -d' ' -f7 | sort | uniq  
9. Llisteu els camps 1 i 3 a 5.  
cat /etc/passwd | tr -s ':', ' ' | cut -s -d' ' -f1,3,4,5  

Feu les següents preguntes amb el fitxer comma.dat

10. Ordena-ho per 'company'.  
cat comma.dat | grep company | sort  
11. Ordena-ho per 'company' ignorant la diferència entre majúscules i minúscules i filtrant que surtin només els únics.  
cat comma.dat | grep company | sort  
12. Llista els 'phone' ordenats alfabèticament.  
cat comma.dat | cut -s -d',' -f5 | sort -d  
13. Com s'ordenarien numèricament?  
cat comma.dat | cut -s -d',' -f5 | sort -n  
14. Presenta per ordre els telèfons majors que 1234567890.  
15. Llista les grandàries i noms de tots els fitxers del directori ordenats per grandària (amb l'ordre sort)  
ls -lh | tr -s ' ',' ' | cut -d' ' -f 9,5 | sort -h  
16. Busqueu en el vostre directori HOME i en tots els seus subdirectoris els fitxers el nom dels quals contingui la lletra 'a'.  
ls -a -R | grep a  